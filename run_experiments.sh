#!/bin/sh

echo "Weclome to SUPERPRAK experiments runner"
echo "2013 Maxim Novikov. Lomonosov MSU, CMC"
echo ""

H=`hostname`
if [ "$H" = "fen1" ]; then
	H=bluegene
fi

echo "Running on $H"


sleep 5

mkdir "results_$H" 2>/dev/null
cd "results_$H" 

# SET FILES LIST YOU WANT HERE
for c in generation shuffle shuffle_large generation_large
do
	mkdir $c 2>/dev/null
	cd $c
	rm *.out 2>/dev/null
	rm *.err 2>/dev/null
	if [ "$H" = "regatta" ]; then
		 mpic++ -fopenmp -o ./main ../../$c.cpp
		 if [ "$?" -ne "0" ]; then
		 	exit 1;
		 fi
		 for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		 do
		 	ompsubmit -n $i -w 2:00 ./main
		 	ompsubmit -n $i -w 2:00 ./main
		 	ompsubmit -n $i -w 2:00 ./main
		 	ompsubmit -n $i -w 2:00 ./main
		 done
	fi
	if [ "$H" = "bluegene" ]; then
		bgxlC_r -qsmp=omp -o ./main ../../$c.cpp
		 if [ "$?" -ne "0" ]; then
		 	exit 1;
		 fi
		for i in 1 2 3 4
		do
			mpisubmit.bg -n 4 -w 00:2:00 -m smp -e "OMP_NUM_THREADS=$i" ./main 	
		done
	fi
	cd ../
done

echo ''
echo ''
echo 'DONE'
echo "Submitted all jobs. Please wait for .out files to appear in 'results' folder"
echo ''


