#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <iostream>
#include <vector>
#include <algorithm>   

/* READ ME!!!
 * This code does exactly the same thing generate.cpp does
 * but many times in order to get longer computations
 * 
 * to understand what is happennning you better see
 * generation.cpp
 */

#define n_runs 5

enum generator_type{RAND, M_RAND}; 

// M_RAND - our thread-safe imlementation
// RAND - standatr rand

#define default_generator M_RAND // set genetator type here
#define debug_mode 0 // set 1 if you want to see debug information
#define show_stat 1  // set 1 if you want to see array statistics

//set probabilities here
void init_probabilities(std::vector<int> &probabilities){

	probabilities.push_back(100000);
	probabilities.push_back(3000000);
	probabilities.push_back(300000);
	probabilities.push_back(1900000);

}

// NOTHING TO SEE BELOW. ALL PARAMETERS ARE SET IN THE BEGINING
// USE defines above to select verbosity, and generator type
// USE init probabilities to changr statistics







class M_randGenerator {
	long s,max;
public:
	M_randGenerator(){
		s = rand();
		max=RAND_MAX;
	}
	long rand_range(long max_val){
		return (s = (1664525*s + 1013904223)%max)%max_val;
	}
};

int rand_range(int max_val){
	return rand()%max_val;
}

std::vector <int> get_array_stats(const std::vector <int> &inarr, int maxnum){
	std::vector <int> stats(maxnum);
	for (int i=0; i<int(inarr.size());++i)
		++stats[inarr[i]];
	return stats;
}


std::vector <double> get_hypergeometric(int M,int mu,int D){
	std::vector <double>result(std::min(mu,D));	
	double GHM_zero=1;//GHM(D,M,mu;0)
	for (int i=0; i<mu;i++)
		GHM_zero=GHM_zero*double(M-D-i)/double(M-i);
	result[0]=(GHM_zero);
	for (int i=1; i< int(result.size());++i){
		result[i]=result[i-1]* (double(D-i+1)*double(mu-i+1)/(double(M-mu-D+i)*double(i)));
	}
	return result;
}

void split_probabilities(
				std::vector<int> initial_probabilities, // need to copy since we change it
				std::vector<std::vector<int> > &probabilities){
				
	int full_len = 0,
		n_threads=int(probabilities.size());
	#pragma omp parallel for reduction (+:full_len)
	for(int i=0;i <int(initial_probabilities.size()); i++)
		full_len+=initial_probabilities[i];	
	int mu_most_popular=full_len/n_threads;
	//int mu_last= full_len%mu_most_popular;
	int c_mu=0;
	for (int c_thread = 0; c_thread < int(probabilities.size())-1; ++c_thread){
		int c_full_len=full_len-c_thread*mu_most_popular;
		c_mu=mu_most_popular;
		for (int cur_probab=0; cur_probab<int(initial_probabilities.size());++cur_probab){
			if (c_full_len==0) continue;
			std::vector<double> distrib=get_hypergeometric(c_full_len,c_mu,initial_probabilities[cur_probab]);
			double v = double(rand_range(RAND_MAX))/double(RAND_MAX);
			int res_nym_of_cur_prob=0;
			double d_sum= distrib[0];
			while(d_sum<=v){
				++res_nym_of_cur_prob;
				if(res_nym_of_cur_prob>=int(initial_probabilities[cur_probab]) ||
					res_nym_of_cur_prob>=c_mu)
					break;
				d_sum+=distrib[res_nym_of_cur_prob];
			}
	
			probabilities[c_thread][cur_probab]=res_nym_of_cur_prob;
			c_full_len-=initial_probabilities[cur_probab];
			c_mu-=res_nym_of_cur_prob;
			initial_probabilities[cur_probab]-=res_nym_of_cur_prob;
		}		
	}
	//last cell
	for (int i=0; i<int(initial_probabilities.size());i++)
		probabilities[probabilities.size()-1][i]=initial_probabilities[i];
	return;
}


void split_probabilities_large(
				std::vector<int> initial_probabilities, // need to copy since we change it
				std::vector<std::vector<int> > &probabilities){
	
	int n_threads=int(probabilities.size());
	/*if(n_threads==1){
		probabilities[0] = initial_brobabilities;
		retur
	}*/
	int full_len = 0;
	unsigned int n_unique = initial_probabilities.size();
	
	#pragma omp parallel for reduction (+:full_len)
	for(int i=0;i <int(initial_probabilities.size()); i++)
		full_len+=initial_probabilities[i];	
	
	int mu_most_popular=full_len/n_threads;
	int mu_last= full_len%mu_most_popular;
	
	
	std::vector<int> remaning_spaces(n_threads,mu_most_popular);
	if(mu_last)
		remaning_spaces[n_threads-1] = mu_last;
	
	std::vector<int> split_positions(n_threads-1);
	std::vector<int> n_elems_split(n_threads);
	for(unsigned int i=0;i<n_unique;++i){
#if debug_mode
	std::cout << "splitting probabilities for " << i << std::endl;
#endif	
		for(unsigned int j=0;j<split_positions.size();++j)
			split_positions[j] = rand_range(initial_probabilities[i]);

#if debug_mode
	std::cout << '.';
#endif	

		std::sort(split_positions.begin(),split_positions.end());
		
#if debug_mode
	std::cout << '.';
#endif	

		n_elems_split[0] = split_positions[0];		
		int s=n_elems_split[0];
		for(unsigned int j=1;j<split_positions.size();++j){
			n_elems_split[j] = split_positions[j] - split_positions[j-1];
			s+=n_elems_split[j];
		}
		n_elems_split[n_threads-1] = initial_probabilities[i] - s;
		
#if debug_mode
	std::cout << '.';
#endif	

		for(int t=0;t<n_threads-1;++t){
			int r=0;
			if(n_elems_split[t]>remaning_spaces[t]){
				r = n_elems_split[t] - remaning_spaces[t];
				n_elems_split[t] = remaning_spaces[t];
				n_elems_split[t+1]+=r; 
			}
		}
		
#if debug_mode
	std::cout << '.';
#endif	

		for(int t=n_threads-1;t>0;--t){
			int r=0;
			if(n_elems_split[t]>remaning_spaces[t]){
				r = n_elems_split[t] - remaning_spaces[t];
				n_elems_split[t] = remaning_spaces[t];
				n_elems_split[t-1]+=r; 
			}
		}
		
#if debug_mode
	std::cout << '.';
#endif	

		for(int t=0;t<n_threads;++t){
			probabilities[t][i] = n_elems_split[t];
			remaning_spaces[t]-= n_elems_split[t];
		} 

#if debug_mode
	std::cout << '\n';
#endif	

	}

#if debug_mode
	std::cout << "initial probabilities\n";
	for(unsigned int i=0;i<n_unique;++i)
		std::cout << initial_probabilities[i] << ' ';
	std::cout<< "\nsplited\n";
	for(int i=0;i<n_threads;++i) {
		for(uint j=0;j<n_unique;++j)
			std::cout<<probabilities[i][j]<<' ';
		std::cout<<'\n';
	}
#endif
		
}

				
int generate_one_elem(std::vector<int> &probabilities, int mu){

	int v = rand_range(mu);
	int p=0,c_sum = probabilities[0];
	
	while(c_sum<=v){
		++p;
		if(p>=int(probabilities.size()))
			break;
		c_sum+=probabilities[p];
	}
	return p;
}

int generate_one_elem_m(std::vector<int> &probabilities, int mu, M_randGenerator &R){

	int v = R.rand_range(mu);
	int p=0,c_sum = probabilities[0];
	
	while(c_sum<=v){
		++p;
		if(p>=int(probabilities.size()))
			break;
		c_sum+=probabilities[p];
	}
	return p;
}
void generate_array_prob(
				std::vector<int> &X,
				std::vector<std::vector<int> > &probabilities){

	int n_threads = int(probabilities.size()),
			max_n = int(probabilities[0].size());
	//number of elements in each subarray and it's offset
	std::vector<int> mu, offsets;
	int c_mu=0,s_mu=0;
	for(int i=0;i<n_threads;++i){
		c_mu = 0;
		for(int j=0;j<max_n;++j)
			c_mu+=probabilities[i][j];
		offsets.push_back(s_mu);
		mu.push_back(c_mu);
		s_mu+=c_mu;
	}
	offsets.push_back(s_mu);
	
	#pragma omp parallel for
	for (int i=0;i<n_threads;++i){
#if default_generator == M_RAND
		M_randGenerator R;
#endif
		int c_thread = omp_get_thread_num();
		for(int position = offsets[c_thread]; 
							position < offsets[c_thread+1];++position){
#if default_generator == RAND							
				X[position] = generate_one_elem(probabilities[i],mu[i]);
#else
				X[position] = generate_one_elem_m(probabilities[i],mu[i],R);
#endif
				--mu[i];
				--probabilities[i][X[position]];
		}
	}			
} 

int main(int argc, const char **argv){
	srand (time(NULL));
	std::vector<int> initial_probabilities;	
	init_probabilities(initial_probabilities);

	int n_elems = 0;
	#pragma omp parallel for reduction (+:n_elems)	
	for	(int i=0;i<int(initial_probabilities.size());++i)
		n_elems += initial_probabilities[i];
	std::vector<int> out_array(n_elems);	

	int n_threads = 0;
	#pragma omp parallel
	if(!omp_get_thread_num())
		n_threads = omp_get_num_threads();

	std::cout << "Running on " << n_threads << " threads\n";
	
	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	std::cout<<"programm starts at "<< asctime (timeinfo)<<std::endl;
	  
	double start_time, end_time;


	start_time = omp_get_wtime();
	
	for (int k=0;k<n_runs;++k) {
		// numer of elements in each subarray
		std::vector< std::vector<int> >probabilities(n_threads);
		for (int i=0; i<n_threads;++i)
			probabilities[i] =  std::vector<int>(int(initial_probabilities.size()));

		if(n_elems<1000)
			split_probabilities(initial_probabilities,probabilities);
		else
			split_probabilities_large(initial_probabilities,probabilities); 
	
		generate_array_prob(out_array,probabilities);
	
#if show_stat
		std::cout<<"array stats:"<<std::endl;
		std::vector <int> out_stats=get_array_stats(out_array, initial_probabilities.size());
		for (unsigned int i=0; i<out_stats.size();++i)
			std::cout<<i<<" - "<<out_stats[i]<<std::endl;

#endif
	}
  std::cout<<"work time: "<<end_time-start_time<<std::endl;
	end_time = omp_get_wtime();
#if debug_mode
	std::cout<<"array elems:"<<std::endl;
	for (int i=0;i<n_elems;++i)
		std::cout << out_array[i] << ' ';
	std::cout<<std::endl;
#endif

	return EXIT_SUCCESS;
}


