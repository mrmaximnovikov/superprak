#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <iostream>
#include <vector>

#define debug_mode 1
#define show_stat 1

int rand_range(int max_val){
	return rand()%max_val;
}

std::vector <int> get_array_stats(const std::vector <int> &inarr, int maxnum){
	std::vector <int> stats(maxnum);
	for (int i=0; i<int(inarr.size());++i)
		++stats[inarr[i]];
	return stats;
}


void shuffle_array(std::vector<int> &input_array){
	 std::vector <omp_lock_t> locks_array;
	 locks_array.resize(input_array.size());
	 #pragma omp parallel for
	 for (int i=0; i<locks_array.size(); i++)
        omp_init_lock(&(locks_array[i]));

	 #pragma omp parallel for
	 for (int i=0; i<input_array.size()-1; i++){
		  int offset_change=rand_range(input_array.size()-i);
		  if (offset_change!=0){
			omp_set_lock(&(locks_array[i]));
			omp_set_lock(&(locks_array[i+offset_change]));
			int tmp=input_array[i];
			input_array[i]=input_array[i+offset_change];
			input_array[i+offset_change]=tmp;
			omp_unset_lock(&(locks_array[i]));
			omp_unset_lock(&(locks_array[i+offset_change]));			
		  }
	 }

	 #pragma omp parallel for
	 for (int i=0; i<locks_array.size(); i++)
        omp_destroy_lock(&(locks_array[i]));

}


int main(int argc, const char **argv){
	std::vector<int> offsets;
	offsets.push_back(0);
	offsets.push_back(1000000);
	offsets.push_back(5000000);
	offsets.push_back(10000000);
	offsets.push_back(20000000);
	std::vector<int> input_array(offsets[offsets.size()-1]);
	
	#pragma omp parallel for
	for (int i=0; i<offsets.size()-1; i++)		
		for (int j=offsets[i];j<offsets[i+1];j++)
			input_array[j]=i;	

	int n_threads = 0;
	#pragma omp parallel
	if(!omp_get_thread_num())
		n_threads = omp_get_num_threads();

	std::cout << "Running on " << n_threads << " threads\n";
	
	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	std::cout<<"shuffle programm starts at "<< asctime (timeinfo)<<std::endl;
	  
	double start_time, end_time;
	start_time = omp_get_wtime();


	shuffle_array(input_array);


	end_time = omp_get_wtime();
    std::cout<<"work time: "<<end_time-start_time<<std::endl;
#if show_stat
	std::cout<<"array stats:"<<std::endl;
	std::vector <int> out_stats=get_array_stats(input_array, offsets.size()-1);
	for (int i=0; i<out_stats.size();++i)
		std::cout<<i<<" - "<<out_stats[i]<<std::endl;
#endif


	return EXIT_SUCCESS;
}
