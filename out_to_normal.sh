#!/bin/sh

echo "Weclome to SUPERPRAK experiments parser"
echo "2013 Maxim Novikov. Lomonosov MSU, CMC"
echo ""

H=`hostname`
if [ "$H" = "fen1" ]; then
	H=bluegene
fi

echo "Running on $H"


sleep 1
rm results.txt

# SET FILES LIST YOU WANT HERE
for c in generation shuffle shuffle_large generation_large
do

	echo "$c RUN RESULTS ON $H" >> results.txt

	for f in results_$H/$c/*.out
	do
			echo "" >> results.txt  
		  cat $f | grep -m 1 -e 'Running on' >> results.txt
		  cat $f | grep 'work time' >> results.txt
	done
	
done

echo "Saved results to 'results.txt'"

